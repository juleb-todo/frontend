import {
  IonButton,
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCol,
  IonGrid,
  IonIcon,
  IonRow,
  useIonActionSheet,
  useIonToast,
} from "@ionic/react";
import { checkmarkCircle, checkmarkCircleOutline } from "ionicons/icons";
import { useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { deleteTodo, updateTodo } from "../services/todo";
import { Todo } from "../types/todo";
import { UpdateTodoModal } from "./modals/UpdateTodoModal";

type Props = {
  data: Todo;
};

export const TodoItem = ({ data }: Props) => {
  const { title, dueDate, id, completed } = data;
  const [showActions] = useIonActionSheet();
  const [isOpenUpdateModal, setIsOpenUpdateModal] = useState(false);
  const [toast] = useIonToast();

  const { mutateAsync: onDeleteTodo } = useMutation(deleteTodo);
  const { mutateAsync: onUpdateTodo } = useMutation(updateTodo(id));
  const queryClient = useQueryClient();

  const handleDelete = async () => {
    await onDeleteTodo(id, {
      onSuccess() {
        toast({
          message: "Delete todo successfully",
          duration: 2000,
          color: "success",
        });
        queryClient.invalidateQueries("todo");
      },
    });
  };

  const handleUpdate = () => {
    setIsOpenUpdateModal(true);
  };

  const handleToggleComplete = async () => {
    await onUpdateTodo(
      { completed: !completed },
      {
        onSuccess() {
          queryClient.invalidateQueries(["todo"]);
        },
      }
    );
  };

  const handleClickItem = () => {
    showActions({
      header: "Todo Actions",
      buttons: [
        {
          text: "Update",
          handler: handleUpdate,
        },
        {
          text: "Delete",
          role: "destructive",
          handler: handleDelete,
        },
        {
          text: "Cancel",
          role: "cancel",
        },
      ],
    });
  };

  return (
    <>
      <IonCard>
        <IonGrid className="ion-no-padding">
          <IonRow className="ion-no-padding">
            <IonCol className="ion-no-padding" onClick={handleClickItem}>
              <IonCardHeader>
                <IonCardTitle>{title}</IonCardTitle>
                <IonCardSubtitle>{dueDate}</IonCardSubtitle>
              </IonCardHeader>
            </IonCol>
            <IonCol className="ion-no-padding" size="auto">
              <IonButton fill="clear" onClick={handleToggleComplete}>
                <IonIcon
                  slot="icon-only"
                  icon={completed ? checkmarkCircle : checkmarkCircleOutline}
                  color={completed ? "success" : "medium"}
                />
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonCard>
      <UpdateTodoModal
        todoId={id}
        isOpen={isOpenUpdateModal}
        onClose={() => setIsOpenUpdateModal(false)}
      />
    </>
  );
};
