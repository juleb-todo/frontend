import { IonHeader, IonProgressBar, IonTitle, IonToolbar } from "@ionic/react";

type Props = {
  title: string;
  isLoading?: boolean;
};

export const Header = ({ isLoading, title }: Props) => {
  return (
    <IonHeader>
      <IonToolbar color="primary">
        <IonTitle>{title}</IonTitle>
        {isLoading && <IonProgressBar type="indeterminate" color="light" />}
      </IonToolbar>
    </IonHeader>
  );
};
