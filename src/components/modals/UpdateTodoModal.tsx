import { yupResolver } from "@hookform/resolvers/yup";
import {
  IonButton,
  IonButtons,
  IonContent,
  IonDatetime,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonModal,
  IonNote,
  IonProgressBar,
  IonTitle,
  IonToolbar,
  useIonToast,
} from "@ionic/react";
import { useEffect, useRef } from "react";
import { useForm } from "react-hook-form";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { getTodoById, updateTodo } from "../../services/todo";
import { UpdateTodoFormData, updateTodoSchema } from "../../types/todo";

type Props = {
  todoId: string;
  isOpen: boolean;
  onClose: () => void;
};

export const UpdateTodoModal = ({ todoId, isOpen, onClose }: Props) => {
  const modalRef = useRef<HTMLIonModalElement>(null);
  const [toast] = useIonToast();

  const { data, isLoading: isLoadingData } = useQuery(
    ["todo", todoId],
    getTodoById(todoId)
  );
  const queryClient = useQueryClient();
  const { mutateAsync: onUpdateTodo, isLoading: isSubmitting } = useMutation(
    updateTodo(todoId)
  );

  const {
    register,
    setValue,
    getValues,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<UpdateTodoFormData>({
    resolver: yupResolver(updateTodoSchema),
  });

  useEffect(() => {
    data && reset({ ...data, dueDate: new Date(data.dueDate).toISOString() });
  }, [data, reset, isOpen]);

  const handleDismiss = () => {
    onClose();
  };

  const handleSetDueDate = (value: string) => {
    setValue("dueDate", value);
  };

  const handleUpdateTodo = handleSubmit(async (data) => {
    await onUpdateTodo(data, {
      onSuccess() {
        queryClient.invalidateQueries("todo");
        toast({
          message: "Update todo successfully",
          duration: 2000,
          color: "success",
        });
        handleDismiss();
      },
    });
  });

  const today = new Date().toISOString();
  return (
    <IonModal ref={modalRef} isOpen={isOpen}>
      <IonContent>
        <IonToolbar>
          <IonTitle>Update todo</IonTitle>
          <IonButtons slot="end">
            <IonButton
              color="primary"
              onClick={handleDismiss}
              disabled={isSubmitting}
            >
              Close
            </IonButton>
          </IonButtons>
          {(isLoadingData || isLoadingData) && (
            <IonProgressBar type="indeterminate" />
          )}
        </IonToolbar>
        <IonList>
          <IonItem className={`${!errors.title ? "ion-valid" : "ion-invalid"}`}>
            <IonLabel position="stacked">Title</IonLabel>
            <IonInput
              clearInput
              placeholder="Enter todo title"
              {...register("title")}
            />
            <IonNote slot="error" color="danger">
              {errors.title?.message}
            </IonNote>
          </IonItem>
        </IonList>
        <IonToolbar>
          <IonDatetime
            presentation="date"
            min={today}
            value={getValues("dueDate")}
            onIonChange={({ detail }) => {
              handleSetDueDate(detail.value as string);
            }}
          />
        </IonToolbar>
        {errors.dueDate && (
          <IonNote color="danger" className="ion-margin-horizontal">
            {errors.dueDate?.message}
          </IonNote>
        )}
        <IonButton
          expand="block"
          className="ion-margin"
          onClick={handleUpdateTodo}
          disabled={isSubmitting}
        >
          Submit
        </IonButton>
      </IonContent>
    </IonModal>
  );
};
