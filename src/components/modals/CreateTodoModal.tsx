import { yupResolver } from "@hookform/resolvers/yup";
import {
  IonButton,
  IonButtons,
  IonContent,
  IonDatetime,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonModal,
  IonNote,
  IonProgressBar,
  IonTitle,
  IonToolbar,
  useIonToast,
} from "@ionic/react";
import { useRef } from "react";
import { useForm } from "react-hook-form";
import { useMutation, useQueryClient } from "react-query";
import { addTodo } from "../../services/todo";
import { CreateTodoFormData, createTodoSchema } from "../../types/todo";

export const CreateTodoModal = () => {
  const modalRef = useRef<HTMLIonModalElement>(null);
  const [toast] = useIonToast();
  const queryClient = useQueryClient();
  const { mutateAsync: onAddToDo, isLoading } = useMutation(addTodo);

  const {
    register,
    setValue,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<CreateTodoFormData>({
    resolver: yupResolver(createTodoSchema),
  });

  const handleDismiss = () => {
    modalRef.current?.dismiss();
  };

  const handleSetDueDate = (value: string) => {
    setValue("dueDate", value);
  };

  const handleAddTodo = handleSubmit(async (data) => {
    await onAddToDo(data, {
      onSuccess() {
        queryClient.invalidateQueries("todo");
        toast({
          message: "Create todo successfully",
          duration: 2000,
          color: "success",
        });
        reset({ title: "", dueDate: "" });
        handleDismiss();
      },
    });
  });

  const today = new Date().toISOString();
  return (
    <>
      <IonButton color="tertiary" id="open-modal">
        + Add
      </IonButton>
      <IonModal ref={modalRef} trigger="open-modal">
        <IonContent>
          <IonToolbar>
            <IonTitle>Create new todo</IonTitle>
            <IonButtons slot="end">
              <IonButton
                color="primary"
                onClick={handleDismiss}
                disabled={isLoading}
              >
                Close
              </IonButton>
            </IonButtons>
            {isLoading && <IonProgressBar type="indeterminate" />}
          </IonToolbar>
          <IonList>
            <IonItem
              className={`${!errors.title ? "ion-valid" : "ion-invalid"}`}
            >
              <IonLabel position="stacked">Title</IonLabel>
              <IonInput
                clearInput
                placeholder="Enter todo title"
                {...register("title")}
              />
              <IonNote slot="error" color="danger">
                {errors.title?.message}
              </IonNote>
            </IonItem>
          </IonList>
          <IonToolbar className="ion-margin-top">
            <IonDatetime
              presentation="date"
              min={today}
              onIonChange={({ detail }) => {
                handleSetDueDate(detail.value as string);
              }}
            />
            {errors.dueDate && (
              <IonNote color="danger" className="ion-margin-horizontal">
                {errors.dueDate?.message}
              </IonNote>
            )}
          </IonToolbar>
          <IonButton
            expand="block"
            className="ion-margin"
            onClick={handleAddTodo}
            disabled={isLoading}
          >
            Submit
          </IonButton>
        </IonContent>
      </IonModal>
    </>
  );
};
