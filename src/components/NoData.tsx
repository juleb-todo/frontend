import { IonText } from "@ionic/react";

export const NoData = () => {
  return (
    <IonText className="ion-text-center ion-padding" color="medium">
      <p>No Data</p>
    </IonText>
  );
};
