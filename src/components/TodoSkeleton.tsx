import { IonItem, IonLabel, IonList, IonSkeletonText } from "@ionic/react";

type Props = {
  count?: number;
};

export const TodoSkeleton = ({ count = 1 }: Props) => {
  const items = [...Array(count)];

  return (
    <>
      {items.map((_, index) => (
        <IonList key={index}>
          <IonItem>
            <IonLabel>
              <h3>
                <IonSkeletonText animated style={{ width: "30%" }} />
              </h3>
              <p>
                <IonSkeletonText animated style={{ width: "60%" }} />
              </p>
              <p>
                <IonSkeletonText animated style={{ width: "60%" }} />
              </p>
            </IonLabel>
          </IonItem>
        </IonList>
      ))}
    </>
  );
};
