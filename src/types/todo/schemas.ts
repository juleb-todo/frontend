import * as yup from "yup";

export const createTodoSchema = yup.object({
  title: yup.string().trim().required("Title is required"),
  dueDate: yup.string().required("Due date is required"),
});

export const updateTodoSchema = yup.object({
  title: yup.string().trim().required("Title is required"),
  dueDate: yup.string().required("Due date is required"),
  completed: yup.boolean(),
});

export type CreateTodoFormData = yup.InferType<typeof createTodoSchema>;
export type UpdateTodoFormData = yup.InferType<typeof updateTodoSchema>;
