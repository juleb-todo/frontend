export * from "../todo/schemas";
export type Todo = {
  id: string;
  title: string;
  dueDate: string;
  completed: boolean;
};
