import {
  IonCol,
  IonContent,
  IonGrid,
  IonPage,
  IonRow,
  IonSearchbar,
} from "@ionic/react";
import { useState } from "react";
import { useQuery } from "react-query";
import { Header } from "../components/Header";
import { CreateTodoModal } from "../components/modals/CreateTodoModal";
import { NoData } from "../components/NoData";
import { TodoItem } from "../components/TodoItem";
import { TodoSkeleton } from "../components/TodoSkeleton";
import { getTodoList } from "../services/todo";

export const Home = () => {
  const [searchKey, setSearchKey] = useState("");
  const { data: todoList = [], isLoading } = useQuery(
    ["todo", searchKey],
    getTodoList({ search: searchKey })
  );

  const handleSearchKeyChange = (value: string) => {
    setSearchKey(value);
  };

  return (
    <IonPage>
      <Header title="To Do" isLoading={isLoading} />
      <IonContent>
        <IonGrid>
          <IonRow className="ion-align-items-center">
            <IonCol>
              <IonSearchbar
                debounce={800}
                onIonChange={({ detail }) =>
                  handleSearchKeyChange(detail.value || "")
                }
              />
            </IonCol>
            <IonCol size="auto">
              <CreateTodoModal />
            </IonCol>
          </IonRow>
        </IonGrid>
        {isLoading ? (
          <TodoSkeleton count={2} />
        ) : !todoList.length ? (
          <NoData />
        ) : (
          todoList.map((item) => <TodoItem key={item.id} data={item} />)
        )}
      </IonContent>
    </IonPage>
  );
};
