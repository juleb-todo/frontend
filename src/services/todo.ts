import { CreateTodoFormData, Todo, UpdateTodoFormData } from "../types/todo";
import axios from "axios";
import { API_URL } from "../common/constants";

const endpoint = `${API_URL}/todo`;

export const getTodoList =
  ({ search }: { search?: string }) =>
  async () => {
    const { data } = await axios.get<Todo[]>(endpoint, { params: { search } });
    return data;
  };

export const getTodoById = (id: string) => async () => {
  const { data } = await axios.get<Todo>(`${endpoint}/${id}`);
  return data;
};

export const addTodo = async (body: CreateTodoFormData) => {
  const { data } = await axios.post<Todo>(endpoint, body);
  return data;
};

export const updateTodo =
  (id: string) => async (body: Partial<UpdateTodoFormData>) => {
    const { data } = await axios.patch<Todo>(`${endpoint}/${id}`, body);
    return data;
  };

export const deleteTodo = async (id: string) => {
  const { data } = await axios.delete(`${endpoint}/${id}`);
  return data;
};
